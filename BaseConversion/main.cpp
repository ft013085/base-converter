#include <iostream>
#include <vector>

using namespace std;

// Function to convert a decimal number to a base R number
vector<int> decimalToBaseR(int N, int R) {
    vector<int> baseR;
    while (N > 0) {
        int remainder = N % R; // generate the remainder
        baseR.push_back(remainder); // record the remainder
        N /= R; // update N by the quotient
    }
    return baseR;
}

int main() {
    int N, R;

    cout << "Enter the decimal number: ";
    cin >> N;
    cout << "Enter the base R: ";
    cin >> R;

    vector<int> baseR = decimalToBaseR(N, R);

    cout << "Converted to base " << R << ": ";
    for (int i = baseR.size() - 1; i >= 0; --i) {
        cout << baseR[i];
    }
    cout << endl;

    return 0;
}
